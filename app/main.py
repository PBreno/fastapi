from sqlalchemy.orm.session import Session
from fastapi import FastAPI
import psycopg2
from psycopg2.extras import RealDictCursor
import time
from . import models
from .database import engine
from .routers import post, user, auth

models.Base.metadata.create_all(bind=engine)
app = FastAPI()

while True:

    try:
        conn = psycopg2.connect(host='localhost', database='postgres', user='postgres',
                                password='fastAPI01', cursor_factory=RealDictCursor)
        Cursor = conn.cursor()
        print("Database connectiion was succesfull!")
        break
    except Exception as error:
        print('Connection to database failed')
        print('Error: ', error)
        time.sleep(2)

app.include_router(post.router)
app.include_router(user.router)
app.include_router(auth.router)


@app.get("/")
def root():
    return {"message": "Its funny, but lets do it again!!!"}
